export default function todos() {
  return {
    todos: [],
    inputValue: '',

    fetchTodos: function () {
      fetch('https://jsonplaceholder.typicode.com/todos')
        .then((response) => response.json())
        .then((data) => {
          this.todos = data.slice(0, 10);
        });
    },

    init: function () {
      this.fetchTodos();
    },

    addTodo: function () {
      if (!this.inputValue) {
        this.$refs.newTodo.focus();
        return;
      }

      this.todos = [
        ...this.todos,
        {
          id: Date.now(),
          title: this.inputValue,
          completed: false,
        },
      ];

      this.inputValue = '';
      this.$refs.newTodo.focus();
    },

    toggleTodo: function (id) {
      const index = this.todos.findIndex((todo) => todo.id === id);

      if (index === -1) return;

      this.todos = [
        ...this.todos.slice(0, index),
        { ...this.todos[index], completed: !this.todos[index].completed },
        ...this.todos.slice(index + 1),
      ];
    },

    deleteTodo: function (id) {
      this.todos = this.todos.filter((todo) => todo.id !== id);
    },
  };
}
