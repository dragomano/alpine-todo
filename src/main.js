import Alpine from 'alpinejs';
import './style.css';
import todos from './todos';

Alpine.data('todos', todos);
Alpine.start();
